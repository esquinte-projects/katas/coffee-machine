# [Coffee Machine Project](http://simcap.github.io/coffeemachine/)

Exercice proposé par La Combe du Lion Vert

## Lancer le projet

- Installation des dépendances

```
$>npm install
```

- Démarrer l'application

```
$>npm start
```

## Lancer les tests

- Tests

```
$>npm run test
```

- Coverage

```
$>npm run test:cov
```
