describe("Validator", () => {
  let validator;

  beforeEach(() => {
    validator = require("../src/modules/validator");
  });

  describe("validateInputs", () => {
    describe("throwing error", () => {
      it("should throw error when no input", () => {
        expect(() => validator.validateInputs("")).toThrowError(
          "Invalid input, syntax: chocolate|tea|coffee|orange {money = xxx.yy}"
        );
      });

      it("should throw error when no money given", () => {
        expect(() => validator.validateInputs("water")).toThrowError(
          "Invalid input, syntax: chocolate|tea|coffee|orange {money = xxx.yy}"
        );
      });

      it("should throw error when invalid drinkType", () => {
        expect(() => validator.validateInputs("water 0")).toThrowError(
          "Invalid drinkType, choose one of these: chocolate|tea|coffee|orange"
        );
      });

      it("should throw error when invalid money", () => {
        expect(() => validator.validateInputs("chocolate a")).toThrowError(
          "Invalid money given, format: xxx.yy"
        );

        expect(() => validator.validateInputs("chocolate -1")).toThrowError(
          "Invalid money given, format: xxx.yy"
        );

        expect(() => validator.validateInputs("chocolate .1")).toThrowError(
          "Invalid money given, format: xxx.yy"
        );

        expect(() => validator.validateInputs("chocolate ,1")).toThrowError(
          "Invalid money given, format: xxx.yy"
        );

        expect(() => validator.validateInputs("chocolate 0,001")).toThrowError(
          "Invalid money given, format: xxx.yy"
        );
      });

      it("should throw error when invalid number of sugars", () => {
        expect(() => validator.validateInputs("chocolate 0 z")).toThrowError(
          "Invalid number of sugars, choose a positive number if provided"
        );
      });

      it("should throw error when invalid extra hot", () => {
        expect(() => validator.validateInputs("chocolate 0 3 z")).toThrowError(
          "Invalid extra hot, choose either 0 or 1 if provided"
        );
      });
    });

    describe("returning inputs", () => {
      it("should return array of validated inputs", () => {
        let inputs = validator.validateInputs("chocolate 0.3");
        expect(["chocolate", "0.3"]).toEqual(expect.arrayContaining(inputs));

        inputs = validator.validateInputs("chocolate 0.3 3");
        expect(["chocolate", "0.3", "3"]).toEqual(
          expect.arrayContaining(inputs)
        );

        inputs = validator.validateInputs("chocolate 4 3");
        expect(["chocolate", "4", "3"]).toEqual(expect.arrayContaining(inputs));

        inputs = validator.validateInputs("chocolate 4 3 0");
        expect(["chocolate", "4", "3", "0"]).toEqual(
          expect.arrayContaining(inputs)
        );

        inputs = validator.validateInputs("chocolate 4 3 1");
        expect(["chocolate", "4", "3", "1"]).toEqual(
          expect.arrayContaining(inputs)
        );
      });
    });
  });
});
