const readline = require("readline");

describe("Reader", () => {
  let reader;
  let question;

  function setupMock(input) {
    question = jest
      .fn()
      .mockImplementationOnce((_questionTest, cb) => cb(input));
    jest.spyOn(readline, "createInterface").mockReturnValue({
      question,
      close: jest.fn().mockImplementationOnce(() => undefined),
      on: jest.fn().mockImplementationOnce(() => undefined),
    });
  }

  describe("waitForInputs", () => {
    it("should resolve inputs", () => {
      setupMock("chocolate 2");

      reader = require("../src/modules/reader");

      expect(reader.waitForInputs()).resolves.toBe("chocolate 2");
      expect(question).toHaveBeenCalledTimes(1);
    });
  });
});
