describe("Sender", () => {
  let sender;

  beforeEach(() => {
    sender = require("../src/modules/sender");
  });

  describe("sendToDrinkWater", () => {
    describe("chocolate", () => {
      it("should return formatted order", () => {
        // drink + money
        expect(sender.sendToDrinkWater(["chocolate", "0.6"])).toBe("H::");
        expect(sender.sendToDrinkWater(["chocolate", "0.5"])).toBe("H::");
        expect(sender.sendToDrinkWater(["chocolate", "0.4"])).toBe(
          "M:There are 0.10€ missing"
        );
        expect(sender.sendToDrinkWater(["chocolate", "0"])).toBe(
          "M:There are 0.50€ missing"
        );

        // + sugar
        expect(sender.sendToDrinkWater(["chocolate", "0.5", "0"])).toBe("H::");
        expect(sender.sendToDrinkWater(["chocolate", "0.5", "3"])).toBe(
          "H:3:0"
        );

        // + extra hot
        expect(sender.sendToDrinkWater(["chocolate", "0.5", "0", "0"])).toBe(
          "H::"
        );
        expect(sender.sendToDrinkWater(["chocolate", "0.5", "0", "1"])).toBe(
          "Hh::"
        );
        expect(sender.sendToDrinkWater(["chocolate", "0.5", "3", "0"])).toBe(
          "H:3:0"
        );
        expect(sender.sendToDrinkWater(["chocolate", "0.5", "3", "1"])).toBe(
          "Hh:3:0"
        );
      });
    });

    describe("tea", () => {
      it("should return formatted order", () => {
        // drink + money
        expect(sender.sendToDrinkWater(["tea", "0.5"])).toBe("T::");
        expect(sender.sendToDrinkWater(["tea", "0.4"])).toBe("T::");
        expect(sender.sendToDrinkWater(["tea", "0.3"])).toBe(
          "M:There are 0.10€ missing"
        );
        expect(sender.sendToDrinkWater(["tea", "0"])).toBe(
          "M:There are 0.40€ missing"
        );

        // + sugar
        expect(sender.sendToDrinkWater(["tea", "0.4", "0"])).toBe("T::");
        expect(sender.sendToDrinkWater(["tea", "0.4", "3"])).toBe("T:3:0");

        // + extra hot
        expect(sender.sendToDrinkWater(["tea", "0.4", "0", "0"])).toBe("T::");
        expect(sender.sendToDrinkWater(["tea", "0.4", "0", "1"])).toBe("Th::");
        expect(sender.sendToDrinkWater(["tea", "0.4", "3", "0"])).toBe("T:3:0");
        expect(sender.sendToDrinkWater(["tea", "0.4", "3", "1"])).toBe(
          "Th:3:0"
        );
      });
    });

    describe("coffee", () => {
      it("should return formatted order", () => {
        // drink + money
        expect(sender.sendToDrinkWater(["coffee", "0.7"])).toBe("C::");
        expect(sender.sendToDrinkWater(["coffee", "0.6"])).toBe("C::");
        expect(sender.sendToDrinkWater(["coffee", "0.5"])).toBe(
          "M:There are 0.10€ missing"
        );
        expect(sender.sendToDrinkWater(["coffee", "0"])).toBe(
          "M:There are 0.60€ missing"
        );

        // + sugar
        expect(sender.sendToDrinkWater(["coffee", "0.6", "0"])).toBe("C::");
        expect(sender.sendToDrinkWater(["coffee", "0.6", "3"])).toBe("C:3:0");

        // + extra hot
        expect(sender.sendToDrinkWater(["coffee", "0.6", "0", "0"])).toBe(
          "C::"
        );
        expect(sender.sendToDrinkWater(["coffee", "0.6", "0", "1"])).toBe(
          "Ch::"
        );
        expect(sender.sendToDrinkWater(["coffee", "0.6", "3", "0"])).toBe(
          "C:3:0"
        );
        expect(sender.sendToDrinkWater(["coffee", "0.6", "3", "1"])).toBe(
          "Ch:3:0"
        );
      });
    });

    describe("orange", () => {
      it("should return formatted order", () => {
        // drink + money
        expect(sender.sendToDrinkWater(["orange", "0.7"])).toBe("O::");
        expect(sender.sendToDrinkWater(["orange", "0.6"])).toBe("O::");
        expect(sender.sendToDrinkWater(["orange", "0.5"])).toBe(
          "M:There are 0.10€ missing"
        );
        expect(sender.sendToDrinkWater(["orange", "0"])).toBe(
          "M:There are 0.60€ missing"
        );
      });
    });
  });
});
