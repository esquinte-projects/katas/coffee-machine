class Order {
  constructor(drink, sugars, extraHot) {
    this.drink = drink;
    this.sugars = sugars;
    if (this.sugars > 0) {
      this.stick = true;
    }
    this.extraHot = !!+extraHot;
  }

  toString() {
    const type = `${this.drink.type}${this.extraHot ? "h" : ""}`;
    const sugars = this.sugars > 0 ? this.sugars : "";
    const stick = this.stick ? "0" : "";
    return `${type}:${sugars}:${stick}`;
  }
}

module.exports = Order;
