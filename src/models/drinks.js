const DRINKS = Object.freeze({
  tea: {
    type: "T",
    cost: 0.4,
  },
  coffee: {
    type: "C",
    cost: 0.6,
  },
  chocolate: {
    type: "H",
    cost: 0.5,
  },
  orange: {
    type: "O",
    cost: 0.6,
  },
});

module.exports = DRINKS;
