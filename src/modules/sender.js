const Order = require("../models/order");
const DRINKS = require("../models/drinks");

function sendToDrinkWater([drink, money, sugars, extraHot]) {
  const order = new Order(DRINKS[drink], sugars, extraHot);

  const missing = order.drink.cost - +money;
  if (missing > 0) {
    return formatMessage(`There are ${missing.toFixed(2)}€ missing`);
  }

  return formatOrder(order);
}

function formatMessage(str) {
  return `M:${str}`;
}

function formatOrder(order) {
  return order.toString();
}

module.exports = {
  sendToDrinkWater,
};
