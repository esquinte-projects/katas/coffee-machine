const DRINKS = require("../models/drinks");

function validateInputs(inputs) {
  const filteredInput = inputs
    .split(/(\s+)/)
    .filter((el) => el.trim().length > 0);

  if (filteredInput.length < 2) {
    throw new Error(
      "Invalid input, syntax: chocolate|tea|coffee|orange {money = xxx.yy}"
    );
  }

  checkDrinkType(filteredInput);
  checkMoney(filteredInput);

  if (filteredInput.length > 2) {
    checkSugars(filteredInput);
  }

  if (filteredInput.length > 3) {
    checkExtraHot(filteredInput);
  }

  return filteredInput;
}

function checkDrinkType(inputs) {
  const drinkKey = inputs[0];
  const drinkFound = DRINKS[drinkKey];

  if (!drinkFound) {
    throw new Error(
      "Invalid drinkType, choose one of these: chocolate|tea|coffee|orange"
    );
  }
}

function checkMoney(inputs) {
  const money = inputs[1];
  if (!money.match(/^\d+(\.\d{1,2})?$/)) {
    throw new Error("Invalid money given, format: xxx.yy");
  }
}

function checkSugars(inputs) {
  const sugars = inputs[2];
  if (!sugars.match(/^\d+$/)) {
    throw new Error(
      "Invalid number of sugars, choose a positive number if provided"
    );
  }
}

function checkExtraHot(inputs) {
  const extraHot = inputs[3];
  if (!extraHot.match(/^[0-1]$/)) {
    throw new Error("Invalid extra hot, choose either 0 or 1 if provided");
  }
}

module.exports = {
  validateInputs,
};
