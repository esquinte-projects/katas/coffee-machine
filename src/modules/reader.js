const readline = require("readline");

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.on("close", () => {
  console.log("\nClosing program...");
  process.exit(0);
});

function waitForInputs() {
  return new Promise((resolve) => {
    rl.question("Waiting for inputs: ", (input) => {
      resolve(input);
    });
  });
}

module.exports = {
  waitForInputs,
};
