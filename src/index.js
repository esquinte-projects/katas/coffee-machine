const reader = require("./modules/reader");
const validator = require("./modules/validator");
const sender = require("./modules/sender");

(async () => {
  while (true) {
    try {
      const inputs = await reader.waitForInputs();
      const validatedInputs = validator.validateInputs(inputs);
      const message = sender.sendToDrinkWater(validatedInputs);
      console.log(`message sent: ${message}`);
    } catch (err) {
      console.error(err.message);
    }
  }
})();
